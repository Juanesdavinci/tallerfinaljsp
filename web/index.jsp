<%-- 
    Document   : ejercicio2
    Created on : 16-sep-2018, 12:16:34
    Author     : JuanEsteban
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <title>Ejercicio 2</title>
    </head>
    <body>
         <div class="container">
        <h1>Taller Final</h1>
        <p>Se pretende realizar un aplicativo (con POO) para el manejo del 
            alquiler de un auto en una agencia de alquileres de autos. El 
            usuario debe de ingresar los datos del auto (marca, modelo, color y 
            kilómetros recorridos). Después debe de ingresar los días que va a 
            alquilar el auto para luego calcular el valor que debe de cancelar el
            cliente teniendo en cuenta lo siguiente:</p>
        <ul><li>
                Si los kilómetros recorridos son menos de 50000 el valor del alquiler
                por día es de $150000 (Nota: si el usuario ingreso 0 o menos 
                kilómetros el programa no le debe de permitir calcular el valor, 
                debe de visualizar un mensaje de error).
            </li>
            <li>
                Si los kilómetros recorridos son entre 50000 y 100000 el valor 
                del alquiler por día es de $80000
            </li>
            <li>
                Si son más de 100000 kilómetros recorridos el valor del alquiler por día es de $50000
            </li>
        </ul>
        
       
        
       <form action="LogicaAlquiler" method="POST" class="row">
           <p>Ingrese los datos del Cliente</p>
           <div class="form-group col-5">
               <p>   <label>
                Cédula <input name="cedula" class="form-control" type="text">
            </label></p>
            <div class="form-group col-5">
               <p>   <label>
                Nombre <input name="nombre" class="form-control" type="text">
            </label></p>
           </div>
           
           
           <p>Ingrese los datos del auto</p>
            <div class="form-group col-5">
                <p>   <label>
                Marca:
                <select name="marca" class="form-control">
                    <option>Mazda</option>
                    <option>Renault</option>
                    <option>Hyundai</option>
                    <option>Kia</option>
                    <option>VolksWagen</option>
                    <option>Ford</option>
                    <option>Chevrolet</option>
                </select>
            </label></p>
            <p>   <label>
                Modelo: <input name="modelo" min="0" max="2019" class="form-control" type="number">
            </label></p>
            <p>   <label>
                Color: 
                <select name="color" class="form-control">
                    <option>Rojo</option>
                    <option>Azul</option>
                    <option>Plateado</option>
                    <option>Negro</option>
                    <option>Blanco</option>
                    <option>Verde</option>
                    <option>Marrón</option>
                </select>
            </label></p>
            <p>   <label>
                Kilómetros recorridos: <input name="km" min="0" class="form-control" type="number">
            </label></p>
                <br>
            <input value="enviar" class="btn btn-info" type="submit">
           </div><div class="form-group col-5">
                <p>   <label>Días de alquiler: 
                        <input type="number" name="dias" class="form-control">
            </label></p>
           </div>
       </form>
        </div>
    </body>
</html>
