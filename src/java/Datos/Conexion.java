package Datos;

import java.sql.*;

public class Conexion {
    
    private String bd;
    private String dsn;
    
    private Connection cn;
    private Statement s;
    private ResultSet rs;
    
    private CallableStatement cstmt;
    public String Error;
    
    public Conexion(){
        
        bd="Almacen";
        //dsn="jdbc:sqlserver://SALAN502-5;databaseName=" + bd + ";user=sa;password=Je123456";
        dsn="jdbc:mysql://localhost/AlquileVehiculos?"
                            + "user=root&password=root";
    }
    
    public void abrirConexion(){
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            cn= DriverManager.getConnection(dsn);
        }catch(Exception ex){
            Error = ex.getMessage();
        }
    }
    
    public ResultSet ConsultarBD(String ced){
        try{
            s = cn.createStatement();
            rs = s.executeQuery ("select id, nombre from Cliente where Cedula = " + ced);
            rs.next();
            return rs;
        }catch(Exception ex){
            Error = ex.getMessage();
            return null;
        }
    }
    
    public boolean InsertarBD(String ced, String nom){
        
        try{
            int filaguardada;
            s = cn.createStatement();
           filaguardada = s.executeUpdate("Insert into Cliente values('" + ced + "', '" + nom + "')");
           //filaguardada = cstmt.executeUpdate();
           if (filaguardada == 1){
               return true;
           }else{
               
               return false;
           }
        }catch(Exception ex){
            Error = ex.getMessage();
            return false;
        }
    }
    public void cerrarConexion(){
        try{
           cn.close();
        }catch(Exception e){
            Error = e.getMessage();
        }
    }

    public String ConsultarBD(int id) {
        try{
            s = cn.createStatement();
            rs = s.executeQuery ("SELECT * FROM `Vehiculo` WHERE `id` = " + id);
            rs.next();
            return rs.getString(1);
        }catch(Exception ex){
            return null;
        }
    }

    public boolean InsertarBD(int idCliente, int idVehiculo, int km, double valor) {
        try{
            int filaguardada;
            s = cn.createStatement();
           filaguardada = s.executeUpdate("INSERT INTO `Alquiler`(`idCliente`, "
                   + "`idVehiculo`, `Kilometros`, `Valor`) VALUES (" + idCliente + " , " +idVehiculo
            + ", " + km + ", " + valor + ");");
           //filaguardada = cstmt.executeUpdate();
           if (filaguardada == 1){
               return true;
           }else{
               return false;
           }
        }catch(Exception ex){
            Error = ex.getMessage();
            return false;
        }
    }

    public boolean InsertarBD(String Marca, String Modelo, String Color) {
        
        try{
            int filaguardada;
            s = cn.createStatement();
           filaguardada = s.executeUpdate("INSERT INTO `Vehiculo`(`id`, `Marca`, `Modelo`, `Color`) VALUES "
                   + " ('" + Marca + "' , '" + Modelo + "' , '" + Color + "); ");
           //filaguardada = cstmt.executeUpdate();
           if (filaguardada == 1){
               return true;
           }else{
               return false;
           }
        }catch(Exception ex){
            Error = ex.getMessage();
            return false;
        }
    }
}
