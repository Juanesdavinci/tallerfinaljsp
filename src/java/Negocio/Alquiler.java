/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Negocio.AlquilerCarro;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author JuanEsteban
 */
@WebServlet(name = "Alquiler", urlPatterns = {"/Alquiler"})
public class Alquiler extends HttpServlet {

    private AlquilerCarro alquiler;    
    private Cliente cli;

    private String Error;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            
            if(CreateCarRent(request)){
                  //Consultar cliente
                if(GuardarCliente(request)){              
                    alquiler.CalcularValorTotal();
                    //out.println(c.toString());
                    
                    //Insertar vehiculo
                    alquiler.IngresarVehiculo();
                    alquiler.CalcularValorTotal();
                    
                    //Insertar Alquiler
                    alquiler.IngresarAlquiler(cli.getId(), alquiler.getId(), alquiler.getKm(), alquiler.getValorTotal());
                    
                    //Mostrar Alquiler (Valor!)
                    request.setAttribute("totalPago", alquiler.getValorTotal());
                    RequestDispatcher disp = request.getRequestDispatcher("/respuesta.jsp");
                    disp.forward(request, response);
                }
                
                
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
        private boolean CreateCarRent(HttpServletRequest request){
            alquiler = new AlquilerCarro();
        //Obtener la marca
        try{
            String marca = request.getParameter("marca");
            if(!"".equals(marca)){
                    alquiler.setMarca(marca);
            }else{Error = "Por favor digite un marca"; return false;}
        }catch(Exception e){Error = "Por favor digite un nombre"; return false;}
        // Obtener el modelo
        try{
            String modelo = request.getParameter("modelo");
            if(!"".equals(modelo)){
                alquiler.setModelo(modelo);
            }else{Error = "Por favor digite un modelo"; return false;}
        }catch(Exception e){Error = "Por favor digite un modelo"; return false;}
        
        //Obtener el color
        try{
            String color = request.getParameter("color");
            if(!"".equals(color)){
                    alquiler.setColor(color);
            }else{Error = "Por favor ingrese un color"; return false;}
        }catch(Exception e){Error = "Por favor ingrese un color"; return false;}
        // Obtener el kilometraje
        try{
            String km = request.getParameter("km");
            if(!"".equals(km)){
                int kmInt = Integer.parseInt(km);
                if(kmInt > 0){
                    alquiler.setKm(kmInt);
                }else{Error = "Por favor digite un kilometraje válido"; return false;}
            }else{Error = "Por favor digite un kilometraje válido"; return false;}
        }catch(Exception e){Error = "Por favor digite un kilometraje"; return false;}
        
        //Obtener los dias
        try{
            String dias = request.getParameter("dias");
            if(!"".equals(dias)){
                int diasInt = Integer.parseInt(dias);
                if(diasInt > 0){
                    alquiler.setDias(diasInt);
                }else{Error = "Por favor digite una cantida de días válida"; return false;}
            }else{Error = "Por favor digite una cantida de días válida"; return false;}
        }catch(Exception e){Error = "Por favor digite una cantida de días"; return false;}
            
            return true;
    }
        
        private boolean GuardarCliente(HttpServletRequest request){
            cli = new Cliente();
            String cedula = request.getParameter("cedula");
            cli.setCedula(cedula);
            String nombre = request.getParameter("nombre");
            cli.setNombre(nombre);
            return cli.ConsultarCliente();
        }
        
}
