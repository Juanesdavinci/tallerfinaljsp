/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.Conexion;

/**
 *
 * @author JuanEsteban
 */
public class AlquilerCarro {
    
    private int id;
    private String marca;
    private String color;
    private String modelo;
    private int km;
    private int dias;
    
    float valorDia;
    private float valorTotal;

    public AlquilerCarro() {
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @return the km
     */
    public int getKm() {
        return km;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @param km the km to set
     */
    public void setKm(int km) {
        this.km = km;
    }

    /**
     * @return the dias
     */
    public int getDias() {
        return dias;
    }

    /**
     * @param dias the dias to set
     */
    public void setDias(int dias) {
        this.dias = dias;
    }
    
    private void CalcularValorDia(){
        valorDia = (km > 100000) ? 50000 : (km > 50000)? 80000 : 150000;
    }
    
    public void CalcularValorTotal(){
        CalcularValorDia();
        setValorTotal(dias * valorDia);
    }
    
    @Override
    public String toString(){
                return "<p> El automovil de marca: " + marca
                + ", color: " + color 
                + ", modelo: " + modelo 
                + ". Será alquilado en un total de: <b>$" + getValorTotal() + "</b></p>" 
                        + "<p>El precio por día es de: " + valorDia + ", dado que"
                        + " alquiló el automóvil por " + dias + " dias"
                        + " y el automóvil cuenta con " + km + "km de recorrido</p>";
    }
    
    public String ConsultarCarro(int id){
        String nombre;
        Conexion co = new Conexion();
        co.abrirConexion();
        nombre = co.ConsultarBD(id);
        co.cerrarConexion();
        return nombre;
    
    }
    
    
    
    public boolean IngresarAlquiler(int idCliente, int idVehiculo, int km, double valor){
        boolean respuesta;
        Conexion co = new Conexion();
        co.abrirConexion();
        respuesta = co.InsertarBD(idCliente,idVehiculo, km, valor);
        co.cerrarConexion();
        //return co.InsertarBD(Cedula, Nombre);
        return respuesta;
    
    }
    
    public boolean IngresarVehiculo(){
        boolean respuesta;
        Conexion co = new Conexion();
        co.abrirConexion();
        respuesta = co.InsertarBD(marca, modelo,color);
        co.cerrarConexion();
        //return co.InsertarBD(Cedula, Nombre);
        return respuesta;
    
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the valorTotal
     */
    public float getValorTotal() {
        return valorTotal;
    }

    /**
     * @param valorTotal the valorTotal to set
     */
    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }
    
}
