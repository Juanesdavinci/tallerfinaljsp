/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Datos.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JuanEsteban
 */
public class Cliente {
    
    private int id;
    private String Cedula;
    private String Nombre;
       
    public boolean ConsultarCliente(){
        Conexion co = new Conexion();
        co.abrirConexion();
        ResultSet rs = co.ConsultarBD(getCedula());
        //TODO: Retornar un ID
        co.cerrarConexion();
        if(rs == null){
            return false;
        }else{
            try {
                this.Nombre = rs.getString("nombre");
                this.id = rs.getInt("id");
                return true;
            } catch (SQLException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
    }
    
    public boolean IngresarCliente(){
        boolean respuesta;
        Conexion co = new Conexion();
        co.abrirConexion();
        respuesta = co.InsertarBD(getCedula(), getNombre());
        co.cerrarConexion();
        //return co.InsertarBD(Cedula, Nombre);
        return respuesta;
    
    }

    /**
     * @return the Cedula
     */
    public String getCedula() {
        return Cedula;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Cedula the Cedula to set
     */
    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
}
